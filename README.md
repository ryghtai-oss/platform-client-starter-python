# platform-client-starter-python

Let's get started with using the `RyghtClient` from `ryght-platform-sdk`. You can find a Jupyter Notebook to help you get 
started with `ryght-platform-sdk` in an interactive manner.

## Status
[![python-version](https://img.shields.io/badge/Python-3.11.7-brightgreen)](https://www.python.org/)
[![poetry-version](https://img.shields.io/badge/Poetry-1.7.1-brightgreen)](https://python-poetry.org/docs/)


## Table of contents
- [Pre-requisites](#pre-requisites)
- [Quick start](#quick-start)

## Pre-requisites
   - `Python v3.11.7` : You can install python from the official guide link [here](https://www.python.org/downloads/release/python-3117/) 

#### Option 01: ( Recommended )
   
   - `Poetry v1.7.1`: You can install poetry from the official guide as mentioned [here](https://python-poetry.org/docs/)
or just do `pip install poetry`

#### Option 02: ( If poetry is not preferred )
   
   - You also can use `pip` instead of poetry as well, to install `pip`.
    
## Quick start                                   
- Clone the repo in terminal using following command: 
    > ```
    > https://gitlab.com/ryghtai-oss/platform-client-starter-python.git
    > ```
    
    or download it as ```.zip``` and extract it in the desired location.
- Switch to the Repository dir
  >```
  >cd platform-client-starter-python
  >``` 

- Perform poetry install to install the `ryght-platform-sdk` and other dependencies
  >```
  >poetry install
  >``` 
 This repo is based on `v0.3.6` of `ryght-platform-sdk`

This should install all the required libraries and dependencies, set the repo for experimenting / testing the 
`ryght-platform-sdk`


### Using Pip
  Install `ryght-platform-sdk` and its dependencies via Pip
  
  >```
  >pip install -r requirements.txt
  >``` 
  